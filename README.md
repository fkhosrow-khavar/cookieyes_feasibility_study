# App
This app was created as a feasibility study to determine whether CookieYes Cloud platform can be utilized in react apps to comply with privacy laws like Quebec, GDPR etc. For more information refer to Jira ticket [DM-214](https://carboncure.atlassian.net/browse/DM-214).

# Prerequisites
* Make sure you have nodejs environment setup and ready to go.

# Get started
To run the app, navigate to app folder and 
1. Run `npm install` to install dependencies.
2. Run `npm run start` to start the app.
3. Navigate to [http://localhost:3000](http://localhost:3000) to view it in your browser.

# References
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
This project is deployed using GitLab pages. See gitlab configuration file.
This project uses Gitlab runner installed on local machine (shared runners are disabled).
This project includes CookieYes script generated using CookieYes cloud platform.
