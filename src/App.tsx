import React, { useEffect, useState } from 'react';
import './App.css';
import { useCookies } from 'react-cookie';
import {useCookieWatcher} from '@fcannizzaro/react-use-cookie-watcher'

function App() {
  const [cookies, setCookie] = useCookies(['name']);
  const [show, setShow] = useState(true);

  // cookie existence
  const isNotExpired = useCookieWatcher('name', {
    // check for changes every 500ms
    checkEvery: 500
  });

  // cookie value (updated on change)
  const value = useCookieWatcher('name', {
    valueOnly: true
  });

  useEffect(() => {
    setCookie('name', 'test', { path: '/'});
    console.log(`cookie with value ${cookies.name} is installed`);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (isNotExpired && value) {
      setShow(true);
    }
    else {
      setShow(false);
    }
  }, [isNotExpired, value]);

  return (
    <div className="App">
      {show && <h1>Cookie ${value} is enabled</h1>}
    </div>
  );
}

export default App;
